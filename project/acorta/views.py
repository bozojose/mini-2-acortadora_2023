from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import URL, URL2
from django.views.decorators.csrf import csrf_exempt
from urllib.parse import unquote


@csrf_exempt  # Decorador para pasar del csrf a partir de aquí
def index(request):
    if request.method == "GET":
        urls_guardadas = URL.objects.all()  # Recupera todos los objetos
        # para obtener las urls guardadas
        url_context = {'urls_guardadas': urls_guardadas}
        return render(request, "acorta/index.html", url_context)  # Se muestra
        # la pagina principal con el formulario y las urls almacenadas
    elif request.method == "POST":
        # Trabaja con POST como si fuera un diccionario
        url_qs = request.POST['url']  # Sacar el campo url
        short_qs = request.POST['short']  # Sacar el campo short
        if url_qs == "":  # Con qs vacía para la parte de la url
            urls_guardadas4 = URL.objects.all()  # Recupera todos los objetos
            url_qs_context = {'urls_guardadas4': urls_guardadas4}
            return render(request, "acorta/qsvacia.html", url_qs_context, status=400)  # Se da una respuesta con
            # codigo de error bad request y se muestra la pagina para formularios en blanco
        else:   # Si hay URL para acortar en la qs
            contenido = unquote(url_qs)  # URL extraida de qs decodificada (utf-8 y sin simbolos)
            # Manejo de inicios de url varios
            if contenido.startswith("http://www"):
                contenido = "http://" + str(contenido.split("www.")[-1])
            elif contenido.startswith("http://"):
                contenido = "http://" + str(contenido.split("//")[-1])
            elif contenido.startswith("https://www"):
                contenido = "https://" + str(contenido.split("www.")[-1])
            elif contenido.startswith("https://"):
                contenido = "https://" + str(contenido.split("//")[1])
            else:
                if contenido.startswith("www."):
                    # Se tiene en cuenta si empieza por www. para añadir https://
                    contenido = "https://" + \
                                str(contenido.split("www.")[-1])
                else:
                    # Casos con la url del sitio sin nada delante (se añade https://)
                    contenido = "https://" + contenido
            try:  # Ya estaba almacenada la url introducida
                url_real = URL.objects.get(urls=contenido)  # Obtiene el objeto de URL cuyo valor
                # de urls corresponde con el obtenido en contenido, que es la url introducida
                urls_guardadas3 = URL.objects.all()  # Recupera todos los objetos
                url_real_context = {'url_real': url_real, 'urls_guardadas3': urls_guardadas3}
                return render(request, "acorta/urlrepetida.html", url_real_context)  # Muestra la pagina para
                # urls previamente almacenadas
            except URL.DoesNotExist:  # La url introducida es nueva
                if short_qs == "":  # Recurso vacio
                    url_short_vacio = URL2(urls=contenido)  # se crea un objeto URL2 para luego sacar el id
                    url_short_vacio.save()
                    short_qs = URL2.objects.get(urls=contenido).id  # Coge el id, que aumenta secuencialmente
                try:
                    recurso_existente = URL.objects.get(short=short_qs)  # si el recurso ya se ha usado,
                    # no se puede repetir
                    if recurso_existente.short:
                        urls_guardadas6 = URL.objects.all()  # Recupera todos los objetos
                        url_repetido_context = {'urls_guardadas6': urls_guardadas6}
                        return render(request, "acorta/shortrepetido.html", url_repetido_context, status=400)  # Se da una respuesta con
                        # codigo de error bad request y se muestra la pagina para recursos repetidos
                except URL.DoesNotExist:
                    url_nueva = URL(urls=contenido, short=short_qs)  # Se crea un objeto URL al que se le guarda
                    # como valor de urls la nueva url y como short el valor acortado introducido
                    url_nueva.save()  # Se guardan los datos en la base de datos con el método save()
                    urls_guardadas2 = URL.objects.all()  # Recupera todos los objetos
                    url_nueva_context = {'url_nueva': url_nueva, 'urls_guardadas2': urls_guardadas2}
                    return render(request, "acorta/urlnueva.html", url_nueva_context)  # Muestra la
                    # pagina para urls nuevas
    else:
        return render(request, "acorta/errormetodo.html", status=405)  # Se da una respuesta
        # con codigo de error Method not allowed y se muestra la pagina para metodos no soportados


def redirige(request, valor_acortado):  # Redirección
    try:
        url_aux = URL.objects.get(short=valor_acortado)
        return HttpResponseRedirect(url_aux.urls)  # Respuesta con código 302 de redirección
    except URL.DoesNotExist:
        urls_guardadas5 = URL.objects.all()  # Recupera todos los objetos
        url_redirect_context = {'recurso': valor_acortado, 'urls_guardadas5': urls_guardadas5}
        return render(request, "acorta/redireccion.html", url_redirect_context, status=404)  # Se da una respuesta
        # con codigo de error not found y se muestra la pagina para formularios en blanco
