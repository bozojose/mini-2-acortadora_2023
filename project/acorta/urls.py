from django.urls import path
from . import views

urlpatterns=[
	path('',views.index),
	path('<str:valor_acortado>',views.redirige),
]