from django.db import models

class URL(models.Model):
    urls = models.CharField(max_length=32)
    short = models.TextField()

    def __str__(self):
        return 'URL real: ' + self.urls + ', recurso acortado: /' + str(self.short)

class URL2(models.Model):
    urls = models.CharField(max_length=32)


    def __str__(self):
        return 'URL real: ' + self.urls + ', numero secuencial(id): ' + str(self.id)
